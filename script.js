
let a = 2


const getCube = a**3;

console.log(`The cube of ${a} is ${getCube}`);


const address = {
	street: "258 Washington Ave NW",
	state: "California",
	zipcode: 90011
};

console.log(`I live at ${address.street}, ${address.state} ${address.zipcode}`);

const animal = {
	name: "Lolong",
	specie: "saltwater crocodile",
	weight: "1075 kgs",
	length: "20 ft 3 in."
};

console.log(`${animal.name} was a ${animal.specie}. He weighed at ${animal.weight} with a measurement of ${animal.length}`);


const number = [1,2,3,4,5]


number.forEach( numbers => console.log(numbers));

const reducer = (a,b) => a + b;

console.log(number.reduce(reducer));

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed
	}
}

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog)